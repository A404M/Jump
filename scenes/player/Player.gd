extends KinematicBody2D

export var move_speed = 200.0
export var jump_hight := 150.0
export var jump_time_to_peak := 0.4
export var jump_time_to_descent := 0.3

onready var jump_velocity:float = ((2.0*jump_hight)/jump_time_to_peak)*-1.0
onready var slide_velocity:float = (((2.0*jump_hight)/jump_time_to_peak)*-1.0)/2.0
onready var jump_gravity:float = ((-2.0*jump_hight)/(jump_time_to_peak * jump_time_to_peak))*-1.0
onready var fall_gravity:float = ((-2.0*jump_hight)/(jump_time_to_descent * jump_time_to_descent))*-1.0

var velocity := Vector2.ZERO
var direction := Vector2.ZERO
var onJump = true
var jumpLevel := 0.0
var jumpLevelUp := 1.0
var wantsToJump := false
var isFacedLeft:bool = true
onready var maxJumpLevel := 1.0/float($JumpLevelTimer.wait_time)


func _process(delta):
	if getMouseDirection().y <= 0:
		$JumpArrow.rotation = PI
	else:
		$JumpArrow.look_at(get_global_mouse_position())
		$JumpArrow.rotate(deg2rad(90))

func _physics_process(delta):
	velocity.y += get_gravity()*delta
	if not onJump:
		velocity.x = get_input_velocity() * move_speed
	
	if Input.is_action_just_pressed("jump"):
		jumpInit()
	if Input.is_action_just_released("jump"):
		jump()
	
	
	var pastVel = velocity
	velocity = move_and_slide(velocity,Vector2.UP)
	if velocity.x < 0:
		flipCharToLeft(true)
	elif velocity.x > 0:
		flipCharToLeft(false)
	
	if is_zero_approx(velocity.y):
		if wantsToJump:
			if isFacedLeft:
				$AnimationPlayer.play("jump_left")
			else:
				$AnimationPlayer.play("jump_right")
		elif is_zero_approx(velocity.x):
			$AnimationPlayer.play("idle")
		else:
			$AnimationPlayer.play("run")
		onJump = false
	#print(velocity.y)
	#print(get_slide_count())
	#if velocity != Vector2.ZERO:
	#	print("----")
	for i in get_slide_count():
		var collision := get_slide_collision(i)
		if is_zero_approx(collision.normal.y):
			direction = direction.bounce(collision.normal)
			velocity = (pastVel*direction)


func get_gravity() -> float:
	return jump_gravity if velocity.y < 0.0 else fall_gravity

func jumpInit():
	wantsToJump = true
	$JumpLevelTimer.start()
	$JumpLevelBar.value = 0
	$JumpLevelBar.show()
	$JumpArrow.show()
	jumpLevel = 0.0
	jumpLevelUp = 1.0

func _on_JumpLevelTimer_timeout():
	jumpLevel += jumpLevelUp
	if jumpLevel >= maxJumpLevel:
		jumpLevel = maxJumpLevel
	$JumpLevelBar.value = (jumpLevel/float(maxJumpLevel))*100
	if jumpLevel == maxJumpLevel:
		jump()
	#print($JumpLevelBar.value)

func jump():
	wantsToJump = false
	$JumpLevelTimer.stop()
	$JumpLevelBar.hide()
	$JumpArrow.hide()
	if not onJump:
		direction = getMouseDirection()
		velocity = getJumpVelocity(jumpLevel,direction)
		#print(velocity)
		onJump = true
		Input.action_release("jump")


func getJumpVelocity(jumpLev:float,dir:Vector2)->Vector2:
	if dir.y <= 0:
		return Vector2(0,0)
		#print("here")
		#direction.y = -1
		#jumpLev = 0
	#print(dir)
	var vel = Vector2(
			slide_velocity,
			jump_velocity*(0.5+jumpLev/float(maxJumpLevel))
		)
	vel *= dir
	return vel


func getMouseDirection()->Vector2:
	return (global_position-get_global_mouse_position()).normalized()

func get_input_velocity() -> float:
	var horizontal := 0.0
	
	if Input.is_action_pressed("left"):
		horizontal -= 1.0
		flipCharToLeft(true)
	if Input.is_action_pressed("right"):
		horizontal += 1.0
		flipCharToLeft(false)
	
	return horizontal



func flipCharToLeft(toLeft:bool):
	if isFacedLeft == toLeft:
		return
	isFacedLeft = toLeft
	for part in $Character.get_children():
		var sprite:Sprite = part.get_node("Sprite")
		sprite.flip_h = not sprite.flip_h
