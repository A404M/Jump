tool

extends Sprite

func _process(delta:float)->void:
	if getCollisionSize() != getSpriteSize():
		print("changed")
		setCollisionSize(getSpriteSize())
		update()

func getSpriteSize()->Vector2:
	return texture.get_size() if texture != null else Vector2.ZERO

func getCollisionSize():
	return $KinematicBody2D/CollisionShape2D.shape.extents*2

func setCollisionSize(s:Vector2):
	$KinematicBody2D/CollisionShape2D.shape.extents = s/2
